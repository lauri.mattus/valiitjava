package it.vali;

public class Main {

    public static void main(String[] args) {
        float a = 123.34f;
        double b = 123.4343;

        System.out.printf("Arvude %e ja %e summa on %e%n", a, b, a + b);
        System.out.printf("Arvude %f ja %f summa on %f%n", a, b, a + b);
        System.out.printf("Arvude %g ja %g summa on %g%n", a, b, a + b);
        // arvude 123.34f ja 123.4343 summa on


        // Printige jagatis
        float c = 131111f;
        float e = 23111111f;

        System.out.println(c / e);

        double f = 131111;
        double g = 23111111;

        System.out.println(f / g);

        System.out.println(c * e);
        System.out.println(f * g);
    }
}
