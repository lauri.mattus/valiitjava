package it.vali;

public class Main {

    public static void main(String[] args) {
        Car bmw = new Car();
        bmw.startEngine();
        bmw.startEngine();
        bmw.stopEngine();
        bmw.stopEngine();
        bmw.accelerate(100);
        bmw.startEngine();
        bmw.accelerate(100);

        Car fiat = new Car();
        Car mercedes = new Car();
        Car opel = new Car("Opel", "Vectra", 1999, 205);
        opel.startEngine();
        opel.accelerate(205);

        Person person = new Person("Lauri", "Mattus", Gender.MALE, 35);

        Person otherPerson = new Person("Jaan", "Jalakas", Gender.MALE, 23);
        opel.setDriver(person);
        opel.setOwner(otherPerson);
        System.out.printf("Sõiduki %s omaniku vanus on %d%n",
                opel.getMake(), opel.getOwner().getAge());

        opel.setMaxPassengers(4);

        opel.addPassenger(person);
        opel.addPassenger(person);
        opel.addPassenger(person);
        opel.addPassenger(person);
        opel.addPassenger(otherPerson);

        opel.showPassengers();

        opel.removePassenger(otherPerson);
        opel.showPassengers();

        Person juku = new Person("Juku", "Juurikas", Gender.MALE, 23);
        Person malle = new Person("Malle", "Maasikas", Gender.FEMALE, 30);

        System.out.println(juku);
        System.out.println(malle);

        System.out.println(opel);

    }
}
