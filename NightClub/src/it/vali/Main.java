package it.vali;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	    String savedFirstName = "Lauri";
        String savedLastName = "Mattus";

        System.out.println("Tere, kuidas on sinu nimi?");

        Scanner scanner = new Scanner(System.in);

        String guestFirstName = scanner.nextLine();

        if(savedFirstName.toLowerCase().equals(guestFirstName.toLowerCase())) {
            System.out.printf("Tere %s%n", guestFirstName.toUpperCase());

            System.out.println("Kui vana sa oled?");
            int age = Integer.parseInt(scanner.nextLine());
            if(age >= 18) {
                System.out.println("Tere tulemast klubisse?");
            } else {
                System.out.println("Sorry, oled alaealine.");
            }
        }
        else {
            System.out.println("Mis on sinu perekonnanimi?");
            String guestLastName = scanner.nextLine();
            if(guestLastName.equals(savedLastName)) {
                System.out.printf("Tere %s sugulane%n", savedFirstName);
            }
            else {
                System.out.println("Sorry, ma ei tunne sind.");
            }
        }

        // Kõigepealt küsitakse külastaja nime
        // kui nimi on listis, siis öeldakse kasutajel
        // "Teretulemast Lauri"
        // ja küsitakse külastaja vanust
        // Kui kasutaja on alaealine, teavitatakse teda, et sorry
        // sisse ei pääse, muul juhul siis öeldakakse tere tulemast klubisse

        // Kui kasutaja ei olnud listis
        // küsitakse kasutajalt ka tema perekonnanime
        // kui perekonnanimi on listis siis öeldakse teretulemast Lauri Mattus
        // muul juhul öeldakse "ma ei tunne sind"
    }
}
