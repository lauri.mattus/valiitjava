<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  

		<h1>Add New Employee</h1>
       <form:form method="post" action="save?${_csrf.parameterName}=${_csrf.token}" enctype="multipart/form-data">  
      	
      	<table >  
         <tr>  
          <td>Name : </td> 
          <td><form:input id="name" onchange="calculateSalary()" path="name"  /></td>
         </tr>  
         <tr>  
          <td>Salary :</td>  
          <td><form:input id="salary" path="salary" /></td>
         </tr> 
         <tr>  
          <td>Designation :</td>  
          <td><form:input path="designation" /></td>
         </tr> 
         <tr>  
          <td>Department :</td>  
          <td><form:select path="departmentId" items="${departments}" /></td>
         </tr>
         <tr>  
          <td>User :</td>  
          <td><form:select path="userId" items="${users}" /></td>
         </tr>
         <tr>  
          <td>Birthday :</td>  
          <td><form:input type="date" path="birthday" /></td>
         </tr>
		 <tr>  
          <td>Active :</td>  
          <td><form:checkbox path="active" /></td>
         </tr>
         <tr>
         	<td colspan="2">
         	File to upload: <input type="file" name="file">   
         	</td>
         	
         </tr>
         <tr>  
          <td> </td>  
          <td><input id="button" type="submit" value="Save" /></td>  
         </tr>  
        </table>
        
       </form:form>
        <script>
        	document.getElementById("button").value = "Salvesta";
        	function calculateSalary() {
        		if(document.getElementById("name").value == "Peeter") {
        			document.getElementById('salary').value = 2000;
        		}
        		else {
        			document.getElementById('salary').value = 1000;
        		}	
        		
        	}
        </script> 
