package it.vali;

public class Pet extends Animal {
    private String ownerName;

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    @Override
    public void printInfo() {
        super.printInfo();
        System.out.printf("Omaniku nimi: %s%n", ownerName);
    }

}
