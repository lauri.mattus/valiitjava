package it.vali;

public interface Logger {
    void writeLog(String errorMessage);
}
