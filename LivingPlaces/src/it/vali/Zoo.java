package it.vali;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Zoo implements LivingPlace {
    private List<Animal> animals = new ArrayList<Animal>();
    // Siin hoitakse infot, palju meil igat looma farmis on
    private Map<String, Integer> animalCounts = new HashMap<String, Integer>();

    // Siin hoitakse infot, palju meil igat looma farmis mahub
    private Map<String, Integer> maxAnimalCounts = new HashMap<String, Integer>();

    public Zoo() {
        maxAnimalCounts.put("Lion", 2);
        maxAnimalCounts.put("Fox", 3);
    }

    @Override
    public void addAnimal(Animal animal) {
        // Kas animal on tüübist Pet või pärineb sellest tüübist
        if(Pet.class.isInstance(animal)) {
            System.out.println("Lemmikloomi loomaaeda vastu ei võeta");
            return;
        }

        String animalType = animal.getClass().getName();

        if(!maxAnimalCounts.containsKey(animalType))
        {
            System.out.println("Loomaaias üldse sellistele loomadele kohta pole");
            return;
        }

        if(animalCounts.containsKey(animalType)) {
            int animalCount = animalCounts.get(animalType);
            int maxAnimalCount = maxAnimalCounts.get(animalType);
            if(animalCount >= maxAnimalCount) {
                System.out.println("Loomaaias on sellele loomale kõik kohad juba täis");
                return;
            }

            animalCounts.put(animalType, animalCounts.get(animalType) + 1);
            // Sellist looma veel ei ole farmi
            // kindlasti sellele loomale kohta on
        } else {
            animalCounts.put(animalType, 1);

        }
        animals.add(animal);
        System.out.printf("Farmi lisati loom %s%n", animalType);
    }

    @Override
    public void printAnimalCounts() {

    }

    @Override
    public void removeAnimal(String animalType) {

    }
}
