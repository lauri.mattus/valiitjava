package it.vali;

public class Main {

	static Logger logger = new EmailLogger();

    public static void main(String[] args) {

    	try {
			int a = 3 / 0;
		} catch (Exception e) {
    		logger.writeLog("Nulliga jagamise viga tekkis");
		}
	    LivingPlace livingPlace = new Forest();

	    Pig pig = new Pig();

	    livingPlace.addAnimal(new Cat());
	    livingPlace.addAnimal(new Horse());
	    livingPlace.addAnimal(new Pig());
	    livingPlace.addAnimal(pig);
	    livingPlace.addAnimal(new Pig());
	    Cow cow = new Cow();

	    livingPlace.addAnimal(cow);

	    livingPlace.printAnimalCounts();
	    livingPlace.removeAnimal("Cow");
		System.out.println();
		livingPlace.printAnimalCounts();
		livingPlace.removeAnimal("Pig");
		livingPlace.printAnimalCounts();
		livingPlace.removeAnimal("Pig");
		livingPlace.removeAnimal("Pig");


		// Mõelge ja täiendage zoo ja forest klasse nii, et neil oleks nende
		// 3 meetodi sisud

    }
}
