package it.vali;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	    int a = 0;
        try {
            int b = 4 / a;
            String word = null;
            word.length();
        }
        // Kui on mitu Catch plokki, siis otsib ta esimese ploki, mis oskab
        // antud Exception
        catch (ArithmeticException e){
                if(e.getMessage().equals("/ by zero")) {
                    System.out.println("Nulliga ei saa jagada");
                }
                else {
                    System.out.println("Esines aritmeetiline viga");
                }
        }
        catch (RuntimeException e) {
            System.out.println("Esines reaalajas esinev viga");
        }
        // Exception on klass, millest kõik erinevad Exceptioni tüübid
        // pärinevad. Mis omakorda tähendab, et püüdes kinni
        // selle üldise Exceptioni, püüme me kinni kõik Exceptionid
        catch (Exception e) {
           System.out.println("Esines viga");
        }


        // Küsime kasutajalt numbri ja kui number
        // ei ole korrektses formaadis (ei ole tegelikult number), siis ütleme mingi veateate
        Scanner scanner = new Scanner(System.in);

        boolean correctNumber = false;

        do {
            System.out.println("Sisesta number");
            try {
                int number = Integer.parseInt(scanner.nextLine());
                correctNumber = true;
            } catch (NumberFormatException e) {
                System.out.println("Number oli vigane");
            }
        } while (!correctNumber);

        // Loo täisarvude massiiv 5 täisarvuga ning ürita sinna lisada kuues täisarv
        // Näita veateadet.
        int[] numbers = new int[] {1, 2, 3, 4, 5};
        try {
            numbers[5] = 6;
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Indeksit, kuhu tahtsid väärtust määrata, ei eksisteeri");
        }
        catch (Exception e) {
            System.out.println("Juhtus mingi tundmatu viga");
        }

    }
}
