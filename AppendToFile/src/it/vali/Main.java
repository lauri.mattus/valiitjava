package it.vali;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        try {
            FileWriter fileWriter = new FileWriter("output.txt", true);
            fileWriter.append("Tere\r\n");
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // 1. Koosta täisarvude massiiv 10 arvust ning seejärel kirjuta faili kõik suuremad arvud kui 2
        int[] numbers = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 10};

        try {

            FileWriter fileWriter = new FileWriter("numbers.txt");
            for (int i = 0; i < numbers.length; i++) {
                if(numbers[i] > 2) {
                    fileWriter.write(numbers[i]  + "\r\n");
                }
            }
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }




        // 2. Küsi kasutajalt kaks arvu.
        // Küsi seni kuni mõlemad on korrektsed (on numbriks teisendatavad)
        // ning nende summa ei ole paaris arv.
        Scanner scanner = new Scanner(System.in);

        int a = 0;
        int b = 0;
        do {
            boolean correctNumber = false;
            do {
                System.out.println("Sisesta esimene arv");
                try {
                    a = Integer.parseInt(scanner.nextLine());
                    correctNumber = true;
                } catch (NumberFormatException e) {
                   System.out.println("Number oli vales formaadis");
                }
            } while (!correctNumber);

            correctNumber = false;

            do {
                System.out.println("Sisesta teine arv");
                try {
                    b = Integer.parseInt(scanner.nextLine());
                    correctNumber = true;
                } catch (NumberFormatException e) {
                    System.out.println("Number oli vales formaadis");
                }
            } while (!correctNumber);
        } while ((a + b) % 2 == 0);


        // 3. Küsi kasutajalt mitu arvu ta tahab sisestada
        // seejärel küsi ühe kaupa kasutajalt need arvud
        // ning kirjuta nende arvude summa faili, nii et see lisatakse alati juurde (Append)
        // Tulemus on iga programmi käivitamisel järel on failis kõik eelnevad summad kirjas.
        System.out.println("Mitu arvu soovid sisestada?");
        int count = Integer.parseInt(scanner.nextLine());

        int sum = 0;
        for (int i = 0; i < count; i++) {
            System.out.printf("Siseta arv number %d%n", i + 1);
            int number =  Integer.parseInt(scanner.nextLine());
            sum += number;
        }
        try {
            FileWriter fileWriter = new FileWriter("sum.txt", true);
            fileWriter.append(sum + System.lineSeparator());
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
