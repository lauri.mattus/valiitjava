package it.vali;

public class Main {

    public static void main(String[] args) {
        int[] numbers = new int[] { 1, -4, 1, -12, 161, 14 };

        int max = numbers[0];
        for (int i = 1; i < numbers.length; i++) {
            if(numbers[i] > max) {
                max = numbers[i];
            }
        }

        System.out.println(max);

        int min = numbers[0];
        for (int i = 1; i < numbers.length; i++) {
            if(numbers[i] < min) {
                min = numbers[i];
            }
        }
        System.out.println(min);

        // Leia suurim paaritu arv
        // mitmes number see on?

        max = Integer.MIN_VALUE;
        int position = 1;
        boolean oddNumbersFound = false;

        for (int i = 0; i < numbers.length; i++) {
            if(numbers[i] % 2 != 0 && numbers[i] > max) {
                max = numbers[i];
                position = i + 1;
                oddNumbersFound = true;
            }
        }
        if(oddNumbersFound) {
            // Suurim paartitu number on 11 ja ta on järekorras number 5
            System.out.printf("Suurim paartitu number on %d ja ta on järekorras number %d%n", max, position);
        }
        else {
            System.out.println("Paaritud arvud puuduvad");
        }
        // Kui paarituid arve üldse ei ole, prindi, et "Paaritud arvud puuduvad"

        max = Integer.MIN_VALUE;
        position = -1;

        for (int i = 0; i < numbers.length; i++) {
            if(numbers[i] % 2 != 0 && numbers[i] > max) {
                max = numbers[i];
                position = i + 1;
            }
        }
        if(position != -1) {
            System.out.printf("Suurim paartitu number on %d ja ta on järekorras number %d%n", max, position);
        }
        else {
            System.out.println("Paaritud arvud puuduvad");
        }
    }
}
