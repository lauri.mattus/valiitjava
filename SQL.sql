﻿-- See on lihtne hello world teksti päring, mis 
-- tagastab ühe rea ja ühe veeru
-- (veerul puudub pealkiri). 
-- Selles veerus ja reas saab olema tekst Hello world

SELECT 'Hello World';

-- Täisarvu saab küsida ilma '' märkideta
SELECT 3; 

-- ei tööta PostgreSQL'is. Näiteks MSSql'is töötab

SELECT 'raud' + 'tee' 

-- Standard CONCAT töötab kõigis erinevates SQL serverites 
SELECT CONCAT('all', 'maa', 'raud', 'tee', 'jaam', '.', 2, 0, 0, 4);

-- Kui tahan erinevaid veege, siis panen väärtustele , vahele
SELECT 'Peeter', 'Paat', 23, 75.45, 'Blond'

-- AS märksõnaga saab anda antud veerule nime
SELECT 
	'Peeter' AS eesnimi,
	'Paat' AS perekonnanimi,
	23 AS vanus, 
	75.45 AS kaal, 
	'Blond' AS juuksevärv

-- Tagastab praeguse kellaja ja kuupäeva mingis vaimisi formaadis	
SELECT NOW();

-- Kui tahan konkreetset osa sellest, näiteks aastat või kuud
SELECT date_part('year', NOW());

-- Kui tahan mingit kuupäeva osa ise ettenatud kuupäevast
SELECT date_part('month', TIMESTAMP '2019-01-01');

-- Kui tahan kellaajast saada näiteks minuteid
SELECT date_part('minutes', TIME '10:10');


-- Kuupäeva formaatimine Eesti kuupäeva formaati
SELECT to_char(NOW(), 'HH24:mi:ss DD.MM.YYYY')

-- Interval laseb lisada või eemaldada mingit ajaühikut
SELECT now() + interval '1 days ago';
SELECT now() + interval '2 centuries 3 years 2 months 1 weeks 3 days 4 seconds';

-- Tabeli loomine
CREATE TABLE student (
	id serial PRIMARY KEY, 
	-- serial tähendab, et tüübiks on int, mis hakkab ühe võrra suurenema
	-- PRIMARY KEY (primaarvõti) tähendab, et see on unikaalne väli tabelis
	first_name varchar(64) NOT NULL, -- EI TOHI tühi olla
	last_name varchar(64) NOT NULL,
	height int NULL, -- TOHIB tühi olla
	weight numeric(5, 2) NULL,
	birthday date NULL
); 
-- Tabelist kõikide ridade kõikide veergude küsimine
-- * siit tähendab, et anna kõik veerud
SELECT * FROM student;

SELECT
	first_name AS eesnimi, 
	last_name AS perekonnanimi,
	-- kuidas saaksin vanuse?
	date_part('year', now()) - date_part('year', birthday) AS vanus
FROM 
	student;

-- Kui tahan filtreerida/otsida mingi tingimuse järgi
SELECT 
	* 
FROM 
	student
WHERE
	height = 180;

-- Anna Peeter Tamm või Mari
SELECT 
	* 
FROM 
	student
 WHERE
 	first_name = 'Peeter' 
 	AND last_name = 'Tamm' 
	OR first_name = 'Mari'	
	
-- Küsi tabelist eesnime ja perekonnanime järgi 
-- mingi Peeter Tamm ja mingi Mari Maasikas	
SELECT 
	* 
FROM 
	student
 WHERE
 	(first_name = 'Peeter' AND last_name = 'Tamm') 
	OR
	(first_name = 'Mari' AND last_name = 'Maasikas')
	
-- Anna mulle õpilased, kelle pikkus jääb 170-180 cm vahele
SELECT 
	* 
FROM 
	student
 WHERE
	height >= 170 AND height <= 180
	
-- Anna mulle õpilased, kes on pikemad kui 170cm või lüheemad kui 150cm
SELECT 
	* 
FROM 
	student
 WHERE
	height >= 170 OR height <= 150
	
-- Anna mulle õpilaste eesnimi ja pikkus, kelle sünnipäev on jaanuaris
SELECT 
	first_name, height 
FROM 
	student
 WHERE
	date_part('month', birthday) = 1

-- Anna mulle õpilased, kelle middle_name on null (määramata)
SELECT 
	* 
FROM 
	student
WHERE
	middle_name IS NULL
	
-- Anna mulle õpilased, kelle middle_name EI OLE null
SELECT 
	* 
FROM 
	student
WHERE
	middle_name IS NOT NULL
	
-- Anna mulle õpilased, pikkus ei ole 180 cm
SELECT 
	* 
FROM 
	student
WHERE
	height <> 180
	
-- Anna mulle õpilased, kelle pikkus on 169, 145 või 180, 210
SELECT 
	* 
FROM 
	student
WHERE
	--height = 169 OR height = 145 OR height = 180 OR height = 210
	height IN (169, 145, 180, 210)

-- Anna mulle õpilased, kelle eesnimi on Peeter, Mari või Kalle
SELECT 
	* 
FROM 
	student
WHERE
	--first_name = 'Peeter' OR first_name = 'Mari' OR first_name = 'Kalle'
	first_name IN ('Peeter', 'Mari', 'Kalle')
	
-- Anna mulle õpilased, kelle eesnimi ei ole Peeter, Mari ega Kalle
SELECT 
	* 
FROM 
	student
WHERE
	first_name NOT IN ('Peeter', 'Mari', 'Kalle')	
	
-- Anna mulle õpilased, kelle sünnikuupäev on kuu esimene, neljas või seitsmes päev
SELECT 
	* 
FROM 
	student
WHERE
	date_part('day', birthday) IN (1, 4, 7)

-- Kõik WHERE võrdlused jätavad välja NULL väärtusega read	
SELECT 
	* 
FROM 
	student
WHERE 
	height != 180

-- Anna mulle õpilased pikkuse järjekorras lühemast pikemaks. 
-- Kui pikkused võrdsed järjest kaalu järgi 	
SELECT 
	* 
FROM 
	student
ORDER BY
	height	

-- Kui tahan tagurpidises järjekorras, siis lisanudb sõna DESC (Descending)
-- Tegelikult on olemas ka ASC (Ascending), mis on vaikeväärtus
SELECT 
	* 
FROM 
	student
ORDER BY
	first_name DESC, weight ASC, last_name 
	
-- Anna mulle vanuse järjekorras vanemast nooremaks õpilaste pikkused, 
-- mis jäävad 160 ja 170 vahele
SELECT 
	height 
FROM 
	student
WHERE
	height >= 160 AND height <= 170
ORDER BY
	birthday
	
-- Kui tahan otsida sõna seest mingit sõnaühendit
SELECT 
	*
FROM 
	student
WHERE
	first_name LIKE 'Ka%' -- eesnimi algab 'Ka'
	OR first_name LIKE '%ee%' -- eesnimi sisaldab 'ee'
	OR first_name LIKE '%aan' -- eesnimi lõppeb 'aan'	
	
-- Tabelisse uute kirjete lisamine
INSERT INTO student
	(first_name, last_name, height, weight, birthday, middle_name)
VALUES
	('Alar', 'Allikas', 172, 80.55, '1980-03-14', NULL),
	('Tiiu', 'Tihane', 171, 55.55, '1994-03-12', 'Tiia'),
	('Mari', 'Maasikas', 166, 56.55, '1984-05-22', 'Marleen')

-- Tabelis kirje muutmine
-- UPDATE lausega peab olema ETTEVAATLIK
-- alati peab kasutama WHERE-i lause lõpus	
UPDATE 
	student
SET
	height = 199,
	weight = 100,
	middle_name = 'John',
	birthday = '1998-04-05'
WHERE 
	id = 20
	
-- Muuda kõigi õpilate pikkus ühe võrra suuremaks	
UPDATE 
	student
SET
	height = height + 1

-- Suurenda hiljem kui 1999 sündinud õpilastel sünnipäeva ühe päeva võrra
UPDATE 
	student
SET
	birthday = birthday + interval '1 days'
WHERE
	date_part('year',birthday) > 1999
	
-- Kustutamisel olla ettevaatlik. ALATI KASUTA WHERE'i	
DELETE FROM
	student
WHERE
	id = 20
	
-- Andmete CRUD operations
-- Create (Insert), Read (Select), Update, Delete

-- Loo uus tabel loan, millel on väljad: 
--amount numeric(11,2), start_date, due_date, student_id  	
CREATE TABLE loan (
	id serial PRIMARY KEY,
	amount numeric(11, 2) NOT NULL,
	start_date date NOT NULL,
	due_date date NOT NULL,
	student_id int NOT NULL
);

-- Lisa neljale õpilase laenud
-- Kahele neist lisa veel 1 laen
INSERT INTO loan
	(amount, start_date, due_date, student_id)
VALUES
	(1000, NOW(), '2020-05-14', 6),
	(2300, NOW(), '2021-05-14', 7),
	(100, NOW(), '2023-05-14', 8),
	(500, NOW(), '2024-05-14', 8),
	(1200, NOW(), '2026-05-14', 9),
	(1400, NOW(), '2030-05-14', 9)
	
-- Anna mulle kõik õpilased koos oma laenudega
-- INNER JOIN on selline tabelite liitmine, kus liidetakse ainult need read
-- kus on võrdsed student.id = loan.student_id
-- ehk need read, kus tabelite vahel on seos.
-- ülejäänud read ignoreeritakse

SELECT
	student.first_name,
	student.last_name,
	loan.amount
FROM
	student
INNER JOIN -- INNER JOIN on vaikimisi join, INNER Sõna võib ära jätta
	loan
	ON student.id = loan.student_id

-- Anna mulle kõik õpilased koos oma laenudega
-- aga ainult sellised laenud, mis on suuremad kui 500
-- järjesta laenu koguse järgi suuremast väiksemaks
SELECT
	student.first_name,
	student.last_name,
	loan.amount
FROM
	student
JOIN
	loan
	ON student.id = loan.student_id
WHERE
	loan.amount > 500
ORDER BY
	loan.amount DESC

-- Loo uus tabel loan_type, milles väljad name ja description
CREATE TABLE loan_type (
	id serial PRIMARY KEY,
	name varchar(30) NOT NULL,
	description varchar(500) NULL	
);

-- Uue välja lisamine olemasolevale tabelile
ALTER TABLE loan
ADD COLUMN loan_type_id int 

-- Tabeli kustutamine
DROP TABLE student;	

-- Lisame mõned laenu tüübid
INSERT INTO loan_type
	(name, description) 
VALUES
	('Õppelaen', 'See on väga hea laen'),
	('SMS laen', 'See on väga väga halb laen'),
	('Väikelaen', 'See on kõrge intressiga laen'),
	('Kodulaen', 'See on madala intressiga laen')
	
-- Kolme tabeli INNER JOIN
SELECT 
	s.first_name, s.last_name, l.amount, lt.name
FROM
	student AS s
JOIN
	loan AS l
	ON s.id = l.student_id
JOIN
	loan_type AS lt
	ON lt.id = l.loan_type_id

-- INNER JOIN puhul joinimiste järjekord ei ole oluline	
SELECT 
	s.first_name, s.last_name, l.amount, lt.name
FROM 
	loan_type AS lt
JOIN
	loan AS l
	ON l.loan_type_id = lt.id
JOIN 
	student AS s
	ON s.id = l.student_id	
	
-- LEFT JOIN puhul võetakse joini esimesest (VASAKUST) tabelist kõik read
-- ning teises (Paremas) tabelis näidatakse puuduvatel kohtadel NULL
SELECT 
	s.first_name, s.last_name, l.amount
FROM
	student AS s
LEFT JOIN
	loan AS l
	ON s.id = l.student_id	
	
-- LEFT JOIN puhul on järejekord väga oluline	
SELECT 
	s.first_name, s.last_name, l.amount, lt.name
FROM
	student AS s
LEFT JOIN
	loan AS l
	ON s.id = l.student_id
LEFT JOIN
	loan_type AS lt
	ON lt.id = l.loan_type_id	

-- Annab kõik kombinatsioonid kahe tabeli vahel	
SELECT 
	s.first_name, st.first_name
FROM
	student AS s
CROSS JOIN
	student AS st
WHERE
	s.first_name != st.first_name
	
-- FULL OUTER Join on sama mis LEFT JOIN + RIGHT JOIN
SELECT 
	s.first_name, s.last_name, l.amount
FROM
	student AS s
FULL OUTER JOIN
	loan AS l
	ON s.id = l.student_id	
	
-- Anna mulle kõigi kasutajate perekonnanimed, kes võtsid smslaenu
-- ja kelle laenu kogus üle 100 euro. Tulemused järjesta laenu võtja vanuse järgi
-- väiksemast suuremaks

SELECT 
	s.last_name
FROM
	student AS s
JOIN
	loan AS l
	ON s.id = l.student_id
JOIN
	loan_type AS lt
	ON lt.id = l.loan_type_id
WHERE
	lt.name = 'SMS laen'
	AND l.amount > 100
ORDER BY
	 s.birthday
	 
-- Aggregate functions
-- Agregaatfunktsiooni selectis välja kutsudes kaob võimalus samas select lauses
-- küsida mingit muud välja tabelist, sest agregaatfunktsiooni tulemus on 
-- alati ainult 1 number ja seda ei saa kuidagi näidata koos väljadega,
-- mida võib olla mitu rida

-- Keskmise leidmine. Jäetakse välja read, kus height on NULL
SELECT 
	AVG(height)
FROM
	student

-- Palju on üks õpilane keskmiselt laenu võtnud
-- Arvestatakse ka neid õpilasi, kes ei ole laenu võtnud (amount on NULL)
SELECT
	AVG(COALESCE(loan.amount, 0))
FROM
	student
LEFT JOIN
	loan
	ON student.id = loan.student_id	
	 
SELECT
	ROUND(AVG(COALESCE(loan.amount, 0)), 0) AS "Keskmine laenusumma",
	MIN(loan.amount) AS "Minimaalne laenusumma",
	MAX(loan.amount) AS "Maksimaalne laenusumma",
	COUNT(*) AS "Kõikide ridade arv",
	COUNT(loan.amount) AS "Laenude arv", -- jäetakse välja read, kus loan.amount on NULL
	COUNT(student.height) AS "Mitmel õpilasel on pikkus"		  
			  
FROM
	student
LEFT JOIN
	loan
	ON student.id = loan.student_id	


-- Kastutades GROUP BY jäävad select päringu jaoks alles vaid need väljad, mis on 
-- GROUP BY's ära toodud (s.first_name, s.last_name)
-- Teisi välju saab ainult kasutada agregaatfunktsioonide sees
SELECT
	s.first_name,
	s.last_name, 
	AVG(l.amount),
	SUM(l.amount),
	MIN(l.amount),
	MAX(l.amount)
FROM 
	student AS s
JOIN
	loan AS l
	ON s.id = l.student_id
GROUP BY
	s.first_name, s.last_name

-- Anna mulle laenude summad laenu tüüpide järgi	
SELECT 
	lt.name, SUM(l.amount)
FROM
	loan AS l
JOIN
	loan_type AS lt
	ON l.loan_type_id = lt.id
GROUP BY
	lt.name

-- Anna mulle laenude summad grupeerituna õpilase ning laenu tüübi kaupa
-- mari õppelaen 2400 (1200+1200)
-- mari väikelaen 2000 
-- jüri õppaenu 2000
SELECT 
	s.first_name, s.last_name, lt.name, SUM(l.amount)
FROM
	student AS s
JOIN
	loan AS l
	ON l.student_id = s.id
JOIN
	loan_type AS lt
	ON lt.id = l.loan_type_id
GROUP BY
	s.first_name, s.last_name, lt.name

-- Anna mulle laenude summad sünniaastate järgi
SELECT 
	date_part('year', s.birthday), SUM(l.amount)
FROM
	student AS s
JOIN 
	loan AS l
	ON s.id = l.student_id
GROUP BY
	date_part('year', s.birthday)
	
	-- Anna mulle laenude, mis ületavad 1000, summad sünniaastate järgi
SELECT 
	date_part('year', s.birthday), SUM(l.amount)
FROM
	student AS s
LEFT JOIN 
	loan AS l
	ON s.id = l.student_id
GROUP BY
	date_part('year', s.birthday), s.first_name
-- HAVING on nagu WHERE, aga peale GROUP BY kasutamist
-- Filtreerimisel saab kasutada ainult neid välju, mis on 
-- GROUP BY's ja agregaatfunktsioone
HAVING
	date_part('year', s.birthday) IS NOT NULL
	AND SUM(l.amount) > 1000 -- anna ainult need read, kus summa oli suurem kui 1000
	AND COUNT(l.amount) = 2 -- anna ainult need read, kus laene kokku oli 2
ORDER BY
	date_part('year', s.birthday)
	
-- Anna mulle mitu laenu mingist tüübist on võetud ning mis on nende summad
-- sms laen 	2 	1000
-- kodulaen 	1 	2300

-- Anna mulle mitu laenu mingist tüübist on võetud ning mis on nende summad
-- sms laen 	2 	1000
-- kodulaen 	1 	2300
SELECT 
	lt.name, COUNT(l.amount), COALESCE(SUM(l.amount), 0)
FROM
	loan_type AS lt
LEFT JOIN
	loan AS l
	ON l.loan_type_id = lt.id
GROUP BY
	lt.name


-- Mis aastal sündinud võtsid suurima summa laene?

SELECT 
	date_part('year', s.birthday), SUM(l.amount)
FROM
	student AS s
JOIN
	loan AS l
	ON l.student_id = s.id
GROUP BY
	date_part('year', s.birthday)
ORDER BY
	 SUM(l.amount) DESC
LIMIT 1

-- Anna mulle õpilaste eesnime esitähe esinemise statistika
-- ehk siis mitme õpilase eesnimi algab mingi tähega
-- m 3
-- a 2
-- l 4

-- Anna mulle õpilaste eesnime esitähe esinemise statistika
-- ehk siis mitme õpilase eesnimi algab mingi tähega
-- m 3
-- a 2
-- l 4

-- mis sõnast, mitmendast tähest ja mitu tähte
-- algab 1st
--SELECT SUBSTRING('kala', 1, 1)

SELECT 
	SUBSTRING(first_name, 1, 1), COUNT(SUBSTRING(first_name, 1, 1)) 
FROM 
	student
GROUP BY
	SUBSTRING(first_name, 1, 1)

-- Subquery or Inner query or Nested query
-- Anna mulle õpilased, eesnimi on keskmise pikkusega õpilaste keskmine nimi
SELECT 
	first_name, last_name, (SELECT weight FROM Student WHERE Id = 8)
FROM
	student
WHERE
	first_name IN 

(SELECT 
	middle_name
FROM
	student
WHERE
	height = (SELECT ROUND(AVG(height)) FROM student))
						   
INSERT INTO employee (first_name, last_name, birthday, middle_name)
SELECT first_name, last_name, birthday, middle_name FROM student ORDER BY birthday DESC LIMIT 2	 						   

-- Teisendamine täisarvuks
SELECT CAST(ROUND(AVG(age)) AS UNSIGNED) FROM emp99