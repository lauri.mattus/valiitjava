package it.vali;

public class Main {

    public static void main(String[] args) {
        // Deklareerime/defineerime tekstitüüpi (String) muutuja (variable)
        // mille nimeks paneme name ja väärtuseks Lauri
        String name = "Lauri";
        String lastName = "Mattus";

        System.out.println("Hello " + name + " " + lastName);
    }
}
