package it.vali;
// void - meetod ei tagasta midagi.
// 			meetodile on võimalik kaasa anda parameetrid,
//			mis pannakse sulgude sisse, eraldadades komaga.
// String[] - tähistab stringi massivi
// args		- massiivi nimi, sisaldab käsurealt kaasa
//			 pandud parameetreid
// System.out.println -	on java meetod,
//			millega saab printida välja rida teksti
// 			See, mis kirjutatakse sulgudesse, prindidakse välja
//			ja tehakse reavahetus
public class Main {

    public static void main(String[] args) {
	    System.out.println("Hello World");
    }
}
